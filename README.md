# Mapplication

Mapplication is a demo app which shows how to read in json data and display the results in a list. Then users are able to select data from the list and pass it to a fragment.
This specific implementation uses city data, which is passed to a shared view model, which is then observed by the map fragment, which updates the view accordingly.
Yes, the name is a pun.

## Getting Started

Simply run the app, the UX is very intuitive.

## Notes

Design was ignored. While UI can be considered part of UX, it has much less of an impact than performance and other factors.  

Commits are somewhat sporadic because of a busy schedule.

Architecture choice was MVVM with a shared view model. The design criteria of 1 activity and multiple fragments made it a very obvious choice.  

The business logic all happens in CityReader, while most of the other components handle UI. CityReader doesn't reference any android dependencies, which is why it's easier to test.

Test Driven Development was used here because of how heavy the business logic was. Mostly Unit Tests since not needing to build and deploy the entire application each time to debug saves time.

MapView was used because it's more customizable, in case of overlays added in the future. Like the button to go back.  

Using a big map isn't really the best idea, but it works because Initial loading time was explicitly mentioned as not a concern.   
While it is a big map, most modern phones can store it in memory. Memory usage is consistent throughout the app lifecycle thanks to viewmodel, so there's no overflow.

CircleCI to build since it's much simpler than Jenkins, and this implementation just runs tests. If this app had an iOS counterpart and/or was going to production, then jenkins would have been a better option. 

## Running the tests

CI has been setup, so tests run automatically on code push. Tests can also be run manually as standard jUnit tests.

## Authors

* **Mohamed Alborati**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Backbase, for providing an opportunity and an extremely convenient recruitment process.
