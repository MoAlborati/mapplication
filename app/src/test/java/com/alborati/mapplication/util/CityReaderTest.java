package com.alborati.mapplication.util;

import com.alborati.mapplication.models.City;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CityReaderTest {
    String rawJson = "";
    List<City> cityList;
    CityReader cityReader;

    @Before
    public void initReader(){
        cityReader = new CityReader();
    }

    @Test
    public void checkIfFileExists() {
        try {
            File root = new File(System.getProperty("user.dir")
                    + File.separator + "src"+ File.separator +"main"+File.separator+"assets"
                    , CityReader.FILE_PATH);
            BufferedReader stream = new BufferedReader(new FileReader(root));
            rawJson = cityReader.readJsonFile(stream);
            Assert.assertFalse(rawJson == null || rawJson.isEmpty());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void checkIfJsonIsClean(){
        checkIfFileExists();
        cityList = cityReader.parseJson(rawJson);
        Assert.assertFalse(cityList == null || cityList.isEmpty());
    }

    @Test
    public void checkIfAbleToIndexCities() {
        checkIfJsonIsClean();
        Map<String, List<City>> index = cityReader.indexCities(cityList);
        Assert.assertFalse(index == null || index.isEmpty() && index.size()<50000); //Should be a big map
    }

    @Test
    public void checkIfIndexIsCorrect() {
        checkIfAbleToIndexCities();
        for (String option: Arrays.asList("A", "aL", "Sydn", "tokyo", "zzzzzzzz", "\nnew")){
            List<City> demo = cityReader.getCities(option);
            for (City city: demo){
                Assert.assertTrue(city.getName().toLowerCase().startsWith(option.toLowerCase()));
            }
        }
    }
}