package com.alborati.mapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.alborati.mapplication.ui.filter.FilterFragment;
import com.alborati.mapplication.ui.loading.LoadingFragment;
import com.alborati.mapplication.ui.map.MapFragment;
import com.alborati.mapplication.util.OnBackPress;

public class MainActivity extends AppCompatActivity {
    public final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, LoadingFragment.newInstance(), TAG)
                    .commitNow();
        }
    }

    //Seperating navigation even though it's duplicate code because there will be a need to configure per route
    public void navigateToMaps() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, MapFragment.newInstance(), TAG)
                .commitNow();
    }

    public void navigateToFilter() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, FilterFragment.newInstance(), TAG)
                .commitNow();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG);
        if (fragment instanceof OnBackPress && ((OnBackPress) fragment).handle()){
            return;
        }
        super.onBackPressed();
    }
}
