package com.alborati.mapplication.util;

//TDD standard exception
public class NotYetImplementedException extends RuntimeException {
    public NotYetImplementedException() {
        super("Not Yet Implemented!");
    }
}
