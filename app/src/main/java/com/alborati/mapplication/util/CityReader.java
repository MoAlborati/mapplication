package com.alborati.mapplication.util;

import android.arch.lifecycle.MutableLiveData;

import com.alborati.mapplication.models.City;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CityReader {
    public static final String FILE_PATH = "cities.json";
    public class Status {
        public String prompt;
        public int percentage;

        Status(String prompt, int percentage){
            this.prompt = prompt;
            this.percentage = percentage;
        }
    }

    public interface MockLiveData{
        void postValue(Status status);
    }

    public MutableLiveData<Status> _progress = new MutableLiveData<>();
    //Not using mockito, might be considered 3rd party?
    MockLiveData progress = status -> {
        try {
            System.out.println(status.prompt+":"+status.percentage);
            _progress.postValue(status);
        } catch (Exception e){
            System.out.println(status.prompt+":"+status.percentage);
        }
    };
    /**
     * Build map of cities at start, since app load time isn't a concern.
     * By preloading list for each query, no need to filter each time text changes.
     */
    private Map<String, List<City>> cityIndex = new HashMap<>();
    public Map<String, List<City>> indexCities(List<City> cityList){
        Map<String, List<City>> index = new HashMap<>();
        index.put("", new ArrayList<>());
        progress.postValue(new Status("Indexing", 0));
        int position = 0;
        float max = cityList.size();
        for (City city: cityList){
            String name = city.getName();
            for (int i = 1; i<name.length()+1; i++){
                String key = name.substring(0, i).toLowerCase();
                List<City> oldList = index.get(key);
                if (oldList == null){
                    oldList = new ArrayList<City>();
                }
                oldList.add(city);
                index.put(key, oldList);
            }
            index.get("").add(city);
            position++;
            int percentage = (int) ((position/(max)) *100);
            progress.postValue(new Status("Indexing", percentage));
        }
        max = index.keySet().size();
        position=0;
        for (String key: index.keySet()){
            Collections.sort(Objects.requireNonNull(index.get(key)), (s1, s2) -> {
                int res =  (s1.getName()).compareTo(s2.getName());
                if (res != 0)
                    return res;
                return (s1.getCountry()).compareTo(s2.getCountry());
            });
            position++;
            int percentage = (int) ((position/(max)) *100);
            progress.postValue(new Status("Sorting", percentage));
        }
        cityIndex = index;
        return cityIndex;
    }

    /**
     * Connect all methods after tests were successful
     * @param inputStream json input stream, from assets in instrumented run, from local in unit
     */
    public void initialize(InputStream inputStream){
        try {
            progress.postValue(new Status("Initializing", 0));
            String raw =
                    readJsonFile(new BufferedReader(new InputStreamReader(inputStream)));
            List<City> unindexedCities = parseJson(raw);
            cityIndex = indexCities(unindexedCities);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readJsonFile(BufferedReader stream) throws IOException {
        BufferedReader br = stream;
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();
        progress.postValue(new Status("Reading File", 0));
        while (line != null) {
            sb.append(line);
            line = br.readLine();
        }
        return sb.toString();
    }

    public List<City> parseJson(String rawJson) {
        Type listType = new TypeToken<List<City>>() {}.getType();
        return new Gson().fromJson(rawJson, listType);
    }

    public List<City> getCities(String query) {
        List<City> index = cityIndex.get(query.toLowerCase());
        return index == null ? new ArrayList<>() : index ;
    }
}
