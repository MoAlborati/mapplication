package com.alborati.mapplication.util;

import android.arch.lifecycle.DefaultLifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.MapView;

/**
 * Connects mapview lifecycle to lifecycle observer callbacks
 */
public class MapLifecycleObserver implements DefaultLifecycleObserver {
    private MapView mapView;

    private MapLifecycleObserver(MapView mapView) {
        this.mapView = mapView;
    }

    public static MapLifecycleObserver from(MapView mapView){
        return new MapLifecycleObserver(mapView);
    };

    @Override
    public void onCreate(@NonNull LifecycleOwner owner) {
        mapView.onCreate(new Bundle());
    }

    @Override
    public void onStart(@NonNull LifecycleOwner owner) {
        mapView.onStart();
    }

    @Override
    public void onResume(@NonNull LifecycleOwner owner) {
        mapView.onResume();
    }

    @Override
    public void onPause(@NonNull LifecycleOwner owner) {
        mapView.onPause();
    }

    @Override
    public void onStop(@NonNull LifecycleOwner owner) {
        mapView.onStop();
    }

    @Override
    public void onDestroy(@NonNull LifecycleOwner owner) {
        mapView.onDestroy();
    }
}
