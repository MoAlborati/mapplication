package com.alborati.mapplication.ui.loading;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alborati.mapplication.MainActivity;
import com.alborati.mapplication.R;
import com.alborati.mapplication.binding.FragmentDataBindingComponent;
import com.alborati.mapplication.databinding.LoadingFragmentBinding;
import com.alborati.mapplication.ui.CityViewModel;
import com.alborati.mapplication.util.CityReader;

import java.io.IOException;

/**
 * Standard loading fragment
 */
public class LoadingFragment extends Fragment {
    private CityViewModel mViewModel;
    private LoadingFragmentBinding binding;
    private DataBindingComponent dataBindingComponent= new FragmentDataBindingComponent(this);

    public static LoadingFragment newInstance() {
        return new LoadingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        this.binding = DataBindingUtil.inflate(
                inflater,
                R.layout.loading_fragment,
                container,
                false,
                dataBindingComponent
        );
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityViewModel.class);
        binding.setViewModel(mViewModel);
        mViewModel.status.observe(this,binding.message::setText);
        mViewModel.progress.observe(this,binding.progress::setText);
        try {
            mViewModel.initFragment(getActivity().getAssets().open(CityReader.FILE_PATH));
            //This is a simple check to make sure cities are loaded, can be improved after implementing progress
            mViewModel.index.observe(this, cityList -> {
                if (cityList != null && !cityList.isEmpty()) {
                    ((MainActivity) getActivity()).navigateToFilter();
                }
            });
            mViewModel.query.postValue("");
        } catch (IOException e) {
            e.printStackTrace();
        }
        binding.executePendingBindings();
    }
}
