package com.alborati.mapplication.ui.filter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.alborati.mapplication.databinding.CityContainerBinding;
import com.alborati.mapplication.models.City;

import java.util.ArrayList;
import java.util.List;

/**
 * Standard Adapter
 */
public class CityAdapter extends RecyclerView.Adapter<CityViewHolder> {
    private List<City> cities = new ArrayList<>();
    private CitySelectedInterface citySelectedInterface;

    /**
     * Get Position of City
     *
     * Used to preserve scroll position across config changes
     * @param city received from VM
     * @return position of the city in view, so we can adjust accordingly
     */
    public int positionOf(City city) {
        return cities.indexOf(city);
    }

    /**
     * Callback interface
     */
    public interface CitySelectedInterface{
        public void selectCity(City city);
    }

    CityAdapter(List<City> cities, CitySelectedInterface citySelectedInterface){
        this.cities = cities;
        this.citySelectedInterface = citySelectedInterface;
    }

    @NonNull
    public CityViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                             int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        CityContainerBinding itemBinding =
                CityContainerBinding.inflate(layoutInflater, parent, false);
        return new CityViewHolder(itemBinding);
    }

    public void onBindViewHolder(@NonNull CityViewHolder holder, int position) {
        City item = getItemForPosition(position);
        holder.itemView.setOnClickListener(view -> {
            if (citySelectedInterface != null)
                citySelectedInterface.selectCity(item);
        });
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return cities == null ? 0 : cities.size();
    }

    private City getItemForPosition(int position) {
        return cities.get(position);
    }

    void setCities(List<City> cities) {
        this.cities = cities;
        notifyDataSetChanged();
    }
}
