package com.alborati.mapplication.ui;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.alborati.mapplication.models.City;
import com.alborati.mapplication.util.CityReader;

import java.io.InputStream;
import java.util.List;

/**
 * This was a serious design decision. I felt that because we mostly use the same data across each
 * fragment, we can use the same view model.
 */
public class CityViewModel extends ViewModel {
    /**
     * This is the object which actually interacts with the data
     */
    private CityReader cityReader = new CityReader();
    public MutableLiveData<String> query = new MutableLiveData<>();
    private boolean initialized = false;
    public MutableLiveData<String> status =
            (MutableLiveData<String>) Transformations.map(cityReader._progress, input -> input.prompt);
    public MutableLiveData<String> progress =
            (MutableLiveData<String>) Transformations.map(cityReader._progress, input ->""+ input.percentage+"%");
    public MutableLiveData<List<City>> index =
            (MutableLiveData<List<City>>) Transformations.map(query, input -> cityReader.getCities(input));

    public City city;

    /**
     * Preloads data asynchronously
     *
     * Need to take inputstream as parameter, since assets should only be accessed from the fragment.
     * By leaving it open to inputstream, I'm able to test it anywhere that can provide that data.
     * @param is is provided by the parent, since it depends based on the environment.
     */
    public void initFragment(InputStream is) {
        if (initialized) return;
        initialized = true;
        Thread thread = new Thread() {
            @Override
            public void run() {
                cityReader.initialize(is);
                query.postValue("");
            }
        };
        thread.start();
    }

    /**
     * 2-way databinding implementation
     *
     * Using viewmodel instead of base observable here, so just manually fowarding the text changes.
     * @param s new string
     * @param start ignored
     * @param count ignored
     * @param after ignored
     */
    public void textChanged(CharSequence s, int start, int count, int after){
        query.postValue(String.valueOf(s));
    }
}
