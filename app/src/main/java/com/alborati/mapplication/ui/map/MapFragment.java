package com.alborati.mapplication.ui.map;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alborati.mapplication.MainActivity;
import com.alborati.mapplication.R;
import com.alborati.mapplication.binding.FragmentDataBindingComponent;
import com.alborati.mapplication.databinding.MapFragmentBinding;
import com.alborati.mapplication.ui.CityViewModel;
import com.alborati.mapplication.util.OnBackPress;

public class MapFragment extends Fragment implements OnBackPress {

    private CityViewModel mViewModel;
    private MapFragmentBinding binding;
    private DataBindingComponent dataBindingComponent= new FragmentDataBindingComponent(this);

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        this.binding = DataBindingUtil.inflate(
                inflater,
                R.layout.map_fragment,
                container,
                false,
                dataBindingComponent
                );
        return binding.getRoot();
    }

    /**
     * Minimal code here thanks to databinding
     *
     * After setting view model, the map will handle it's own lifecycle.
     * @param savedInstanceState ignored, data is retained in VM
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityViewModel.class);
        binding.setViewModel(mViewModel);
        binding.switchToFilter.setOnClickListener(v -> ((MainActivity) getActivity()).navigateToFilter());
        binding.executePendingBindings();
    }

    @Override
    public boolean handle() {
        ((MainActivity) getActivity()).navigateToFilter();
        return true;
    }
}
