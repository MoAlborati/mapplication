package com.alborati.mapplication.ui.filter;

import android.support.v7.widget.RecyclerView;

import com.alborati.mapplication.databinding.CityContainerBinding;
import com.alborati.mapplication.models.City;

/**
 * Standard View Holder
 */
public class CityViewHolder extends RecyclerView.ViewHolder {
    private final CityContainerBinding binding;

    public CityViewHolder(CityContainerBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(City city) {
        binding.setCity(city);
        binding.executePendingBindings();
    }
}
