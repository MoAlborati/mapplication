package com.alborati.mapplication.ui.filter;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alborati.mapplication.MainActivity;
import com.alborati.mapplication.R;
import com.alborati.mapplication.binding.FragmentDataBindingComponent;
import com.alborati.mapplication.databinding.FilterFragmentBinding;
import com.alborati.mapplication.ui.CityViewModel;

public class FilterFragment extends Fragment {
    private CityViewModel mViewModel;
    private FilterFragmentBinding binding;
    private DataBindingComponent dataBindingComponent= new FragmentDataBindingComponent(this);

    public static FilterFragment newInstance() {
        return new FilterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        this.binding = DataBindingUtil.inflate(
                inflater,
                R.layout.filter_fragment,
                container,
                false,
                dataBindingComponent
        );
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityViewModel.class);
        //Standard adapter setup + callback, avoiding databound list adapter since cities aren't changing
        CityAdapter adapter = new CityAdapter(mViewModel.index.getValue(), city -> {
            mViewModel.city = city;
            ((MainActivity) getActivity()).navigateToMaps();
        });
        //Listen for changes in view model
        mViewModel.index.observe(this, cities -> {
            binding.emptyCitiesMessage
                    .setVisibility(cities == null || cities.isEmpty() ? View.VISIBLE : View.GONE);
            adapter.setCities(cities);
        });
        binding.setViewModel(mViewModel);
        binding.cityRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.cityRecycler.setAdapter(adapter);
        //Go to last selected city
        if (mViewModel.city != null){
            binding.cityRecycler.scrollToPosition(adapter.positionOf(mViewModel.city));
        }
        binding.executePendingBindings();
    }
}
