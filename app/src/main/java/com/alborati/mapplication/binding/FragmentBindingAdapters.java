package com.alborati.mapplication.binding;

import android.databinding.BindingAdapter;
import android.support.v4.app.Fragment;

import com.alborati.mapplication.models.City;
import com.alborati.mapplication.util.MapLifecycleObserver;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Attach to each fragment, so they have access to binding adapters
 */
public class FragmentBindingAdapters {
    Fragment fragment;
    public FragmentBindingAdapters(Fragment fragment){
        this.fragment = fragment;
    }

    /**
     * Sync MapView lifecycle with Fragment's
     *
     * Most maps based applications don't support orientation changes because the map needs to be rebuilt.
     * To mitigate that, I'm storing the city so even after rebuild user ends up in same location
     * 
     * @param mapView the map with it's own lifecycle
     * @param city start position retained across config changes
     */
    @BindingAdapter("app:observeMap")
    public void observeMap(MapView mapView, City city){
        fragment.getLifecycle().addObserver(MapLifecycleObserver.from(mapView));
        if (city == null) return;
        LatLng latLng = new LatLng(city.getCoord().getLat(), city.getCoord().getLon());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
        mapView.getMapAsync(googleMap -> {
            googleMap.animateCamera(cameraUpdate);
            googleMap.addMarker(new MarkerOptions().position(latLng));
        });
    }
}