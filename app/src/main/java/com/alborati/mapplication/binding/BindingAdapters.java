package com.alborati.mapplication.binding;

/**
 * Static binding adapters that can be referenced from package
 */
final class BindingAdapters {
    private static final BindingAdapters BindingAdapters;
    static {
        BindingAdapters = new BindingAdapters();
    }
}